/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question1;

/**
 *
 * @author syataco
 */
public class Question1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int start = 2;
        int end = 30;
        boolean flag;

        for (int i = start; i <= end; i++) {
            flag = true;

            for (int j = 2; j <= Math.sqrt(i) && flag; j++) {
                if (i % j == 0) {
                    flag = false;
                }
            }

            if (flag) {
                System.out.println(i);
            }
        }
    }
    
}
